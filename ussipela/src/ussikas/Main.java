package ussikas;

import java.awt.Color;
import javax.swing.JFrame;

public class Main {

	public static void main(String[] args) {   //https://www.tutorialspoint.com/swing/swing_jframe.htm
		JFrame obj = new JFrame();
		Mootor mootor = new Mootor();
		
		obj.setBounds(10,10, 705, 500);
		obj.setBackground(Color.white);
		obj.setResizable(false);
		obj.setVisible(true);
		obj.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		obj.add(mootor);
		
		

	}

}
