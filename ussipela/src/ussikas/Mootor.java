package ussikas;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyListener;
import java.util.Random;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Mootor extends JPanel implements KeyListener, ActionListener {

	public void paint(Graphics g) { // http://stackoverflow.com/questions/13497289/how-to-initialize-graphics-g
		// M�nguala
		g.setColor(Color.LIGHT_GRAY);
		g.fillRect(25, 75, 650, 375);

		// Kunstiteos
		ussikaspilt = new ImageIcon("ussikaspilt.jpg");
		ussikaspilt.paintIcon(this, g, 25, 11);

		// Ussi pikkus, skoor
		g.setColor(Color.WHITE);
		g.setFont(new Font("arial", Font.BOLD, 20));
		g.drawString("Ussi pikkus:" + ussipikkus, 30, 50);

		if (roomab == 0) { // alguskoht
			ussxpikkus[2] = 250;
			ussxpikkus[1] = 275;
			ussxpikkus[0] = 300;

			ussypikkus[2] = 250;
			ussypikkus[1] = 250;
			ussypikkus[0] = 250;

		}

		// Kuidas ussi pea k�itub.
		for (int a = 0; a < ussipikkus; a++) { // https://www.youtube.com/watch?v=_SqnzvJuKiA
												// 25:00
			if (a == 0 && paremal) {
				ussipeaparem = new ImageIcon("ussipeaparem.png");
				ussipeaparem.paintIcon(this, g, ussxpikkus[a], ussypikkus[a]);
			}
			if (a == 0 && vasakul) {
				ussipeavasakul = new ImageIcon("ussipeavasakul.png");
				ussipeavasakul.paintIcon(this, g, ussxpikkus[a], ussypikkus[a]);
			}
			if (a == 0 && alla) {
				ussipeaalla = new ImageIcon("ussipeaalla.png");
				ussipeaalla.paintIcon(this, g, ussxpikkus[a], ussypikkus[a]);
			}
			if (a == 0 && yles) {
				ussipeayles = new ImageIcon("ussipeayles.png");
				ussipeayles.paintIcon(this, g, ussxpikkus[a], ussypikkus[a]);
			}
			if (a != 0) {
				ussikeha = new ImageIcon("ussikeha.png");
				ussikeha.paintIcon(this, g, ussxpikkus[a], ussypikkus[a]);
			}
		}
		kinkpilt = new ImageIcon("kinkpilt.png");

		// Mis juhtub, kui uss korjab kingi �ra.
		if ((kinkxasukoht[xasukoht] == ussxpikkus[0] && kinkyasukoht[yasukoht] == ussypikkus[0])) {

			ussipikkus++;
			xasukoht = random.nextInt(26);
			yasukoht = random.nextInt(15);
		}
		kinkpilt.paintIcon(this, g, kinkxasukoht[xasukoht], kinkyasukoht[yasukoht]);

		// Mis juhtub, kui uss roomab endale otsa.
		for (int i = 1; i < ussipikkus; i++) {
			if (ussxpikkus[i] == ussxpikkus[0] && ussypikkus[i] == ussypikkus[0]) {
				paremal = false;
				vasakul = false;
				yles = false;
				alla = false;

				g.setColor(Color.WHITE);
				g.setFont(new Font("arial", Font.BOLD, 50));
				g.drawString("M�ng l�bi!", 220, 250);

				g.setFont(new Font("arial", Font.BOLD, 20));
				g.drawString("Uuesti alustamiseks vajuta t�hikut.", 175, 290);

			}
		}
		repaint(); // Selle abil ei saa nooleklahvidega edasi m�ngida vaid peab
					// t�hikut kasutama

	}

	private static final long serialVersionUID = 1L;

	int roomab = 0;
	int[] ussxpikkus = new int[200];
	int[] ussypikkus = new int[200];

	boolean vasakul = false;
	boolean paremal = true;
	boolean yles = false;
	boolean alla = false;

	ImageIcon ussipeaparem;
	ImageIcon ussipeayles;
	ImageIcon ussipeaalla;
	ImageIcon ussipeavasakul;
	ImageIcon ussikaspilt;

	// M��rab ussi roomamise kiiruse.
	Timer timer;
	int delay = 75;
	ImageIcon ussikeha;
	int ussipikkus = 3;

	// M��rab �ra, kus kingipakk v�ib tekkida.
	ImageIcon kinkpilt;
	int[] kinkxasukoht = { 25, 50, 75, 100, 125, 150, 175, 200, 225, 250, 275, 300, 325, 350, 375, 400, 425, 450, 475,
			500, 525, 550, 575, 600, 625, 650 };
	int[] kinkyasukoht = { 75, 100, 125, 150, 175, 200, 225, 250, 275, 300, 325, 350, 375, 400, 425 };

	Random random = new Random();
	int xasukoht = random.nextInt(26);
	int yasukoht = random.nextInt(15);

	public Mootor() {

		addKeyListener(this); // https://docs.oracle.com/javase/tutorial/uiswing/events/keylistener.html
		setFocusable(true);
		setFocusTraversalKeysEnabled(false);
		timer = new Timer(delay, this);
		timer.start();

	}

	// Mis juhtub, kui uss roomab vastu m�nguala seina.
	@Override
	public void actionPerformed(ActionEvent e) { //// https://www.youtube.com/watch?v=_SqnzvJuKiA
													//// 39:15
		timer.start();
		if (paremal) {

			for (int r = ussipikkus - 1; r >= 0; r--) {
				ussypikkus[r + 1] = ussypikkus[r];
			}
			for (int r = ussipikkus; r >= 0; r--) {
				if (r == 0) {
					ussxpikkus[r] = ussxpikkus[r] + 25;

				} else {
					ussxpikkus[r] = ussxpikkus[r - 1];
				}
				if (ussxpikkus[r] > 650) {
					ussxpikkus[r] = 25;
				}

			}
			repaint();
		}
		if (vasakul) {
			for (int r = ussipikkus - 1; r >= 0; r--) {
				ussypikkus[r + 1] = ussypikkus[r];
			}
			for (int r = ussipikkus; r >= 0; r--) {
				if (r == 0) {
					ussxpikkus[r] = ussxpikkus[r] - 25;

				} else {
					ussxpikkus[r] = ussxpikkus[r - 1];
				}
				if (ussxpikkus[r] < 25) {
					ussxpikkus[r] = 650;
				}

			}
			repaint();

		}
		if (yles) {
			for (int r = ussipikkus - 1; r >= 0; r--) {
				ussxpikkus[r + 1] = ussxpikkus[r];
			}
			for (int r = ussipikkus; r >= 0; r--) {
				if (r == 0) {
					ussypikkus[r] = ussypikkus[r] - 25;

				} else {
					ussypikkus[r] = ussypikkus[r - 1];
				}
				if (ussypikkus[r] < 75) {
					ussypikkus[r] = 425;
				}

			}
			repaint();
		}
		if (alla) {
			for (int r = ussipikkus - 1; r >= 0; r--) {
				ussxpikkus[r + 1] = ussxpikkus[r];
			}
			for (int r = ussipikkus; r >= 0; r--) {
				if (r == 0) {
					ussypikkus[r] = ussypikkus[r] + 25;

				} else {
					ussypikkus[r] = ussypikkus[r - 1];
				}
				if (ussypikkus[r] > 425) {
					ussypikkus[r] = 75;
				}

			}

		}
	}

	// Ussi liikuma saamine
	@Override
	public void keyPressed(KeyEvent e) { // https://www.youtube.com/watch?v=_SqnzvJuKiA
											// 31:59

		if (e.getKeyCode() == KeyEvent.VK_SPACE) {
			roomab = 0;
			ussipikkus = 3;
			repaint();
		}
		if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
			roomab++;
			paremal = true;
			if (!vasakul) {
				paremal = true;
			} else {
				paremal = false;
				vasakul = true;
			}

			yles = false;
			alla = false;
		}

		{
			if (e.getKeyCode() == KeyEvent.VK_LEFT) {

				roomab++;
				vasakul = true;
				if (!paremal) {
					vasakul = true;
				} else {
					vasakul = false;
					paremal = true;
				}

				yles = false;
				alla = false;
			}

			if (e.getKeyCode() == KeyEvent.VK_UP) {
				roomab++;
				yles = true;
				if (!alla) {
					yles = true;
				} else {
					yles = false;
					alla = true;
				}

				vasakul = false;
				paremal = false;
			}
			if (e.getKeyCode() == KeyEvent.VK_DOWN) {
				roomab++;
				alla = true;
				if (!yles) {
					alla = true;
				} else {
					alla = false;
					yles = true;
				}

				vasakul = false;
				paremal = false;
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}
}
